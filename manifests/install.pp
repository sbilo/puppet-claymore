class claymore::install inherits claymore {

  $source = "https://github.com/nanopool/Claymore-Dual-Miner/releases/download/v${claymore::version}
    /Claymore.s.Dual.Ethereum.Decred_Siacoin_Lbry_Pascal.AMD.NVIDIA.GPU.Miner.v${claymore::version}.-.LINUX.tar.gz";

  $filename = basename($source);

  archive { "${claymore::install_dir}/${filename}":
    ensure       => present,
    extract      => true,
    extract_path => $claymore::install_dir,
    source       => ($::claymore::version <= '10.1') ? {
      true  => $source,
      false => "puppet:///modules/claymore/Claymore.s.Dual.Ethereum.Decred_Siacoin_Lbry_Pascal.AMD.NVIDIA.GPU.Miner.v${
        claymore::version}-.LINUX.tar.gz"
    },
    notify       => Service['claymore']
  }

  ensure_packages(['libcurl3', 'jq'], { 'ensure' => 'present' })

  file { '/usr/bin/claymore-stats':
    ensure  => present,
    mode    => '0755',
    content => template('claymore/claymore-stats.erb')
  }

  tidy { $claymore::install_dir:
    age     => "1d",
    recurse => true,
    matches => ['*_log.txt']
  }

  if ($restart_command) {
    file { '/opt/claymore/reboot.sh':
      ensure  => present,
      mode    => '0755',
      content => $restart_command
    }
  }
}