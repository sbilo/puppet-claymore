# Class: claymore
# ===========================
#
# Full description of class claymore here.
#
# Parameters
# ----------
#
# @param dual_coin
# @param version
# @install_dir
# @target_temp
# @ethereum_pools
# @ethereum_wallet
# @dual_pools
# @dual_wallet
# @zcash_pools
# @zcash_wallet
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class claymore(
  Boolean $enable_dual    = true,
  String $dual_coin       = 'decred',
  String $version                   = '9.7',
  Array[String] $env_vars           = [
    'GPU_FORCE_64BIT_PTR=0',
    'GPU_MAX_HEAP_SIZE=100',
    'GPU_USE_SYNC_OBJECTS=1',
    'GPU_MAX_ALLOC_PERCENT=100',
    'GPU_SINGLE_ALLOC_PERCENT=100'],
  Boolean $enabled                  = true,
  Optional[Integer] $target_temp    = 55,
  Optional[Integer] $stop_temp      = 90,
  Optional[Integer] $start_temp     = 50,
  Optional[Integer] $fan_min        = 60,
  Optional[Integer] $fan_max        = 100,
  Integer $esm                      = 0,
  Integer $erate                    = 1,
  Integer $estale                   = 1,
  Integer $ejobtimeout              = 10,
  Integer $djobtimeout              = 30,
  Integer $retrydelay               = 20,
  Stdlib::AbsolutePath $install_dir = '/opt/claymore',
  Boolean $enable_watchdog          = true,
  String $systemd_unit_after        = 'network.target',
  Optional[String] $systemd_exec_pre = undef,
  Optional[Integer] $cclock         = undef,
  Optional[Integer] $mclock         = undef,
  Optional[String] $email           = 'sander.bilo@gmail.com',
  Optional[String] $epasswd         = 'x',
  Optional[Integer] $etha           = 2,
  Optional[Integer] $allpools       = 1,
  Optional[Integer] $allcoins       = 0,
  Optional[Integer] $asm            = undef,
  Optional[Integer] $dcri           = undef,
  Optional[Boolean] $gser           = false,
  Optional[Array[String]] $gpus     = undef,
  Integer $restart_mode             = 0,
  Optional[String] $restart_command,
  Optional[Array[String]] $epools   = ['eth-eu1.nanopool.org:9999', 'eth-eu2.nanopool.org:9999'],
  Optional[String] $ewallet         = '0x005463668233189e1a954F5BCbA37605C54b7DFD',
  Optional[Array[String]] $dpools   = ['yiimp.eu:4252','yiimp.eu:3252','stratum.decredpool.org:3343'],
  Optional[String] $dwallet         = 'DscLTtfMdJ9mW9UtfrPVWTy6zs8KDfmB4Le',
  Optional[String] $dpasswd,
) {

  contain claymore::install
  contain claymore::config
  contain claymore::service

  Class['::claymore::install']
  -> Class['::claymore::config']
  ~> Class['::claymore::service']
}
