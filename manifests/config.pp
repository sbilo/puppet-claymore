class claymore::config inherits claymore {

  file { "${claymore::install_dir}/epools.txt":
    ensure  => file,
    content => template('claymore/epools.txt.erb'),
    notify  => Service['claymore']
  }

  if($claymore::enable_dual) {
    file { "${claymore::install_dir}/dpools.txt":
      ensure  => file,
      content => template('claymore/dpools.txt.erb'),
      notify  => Service['claymore']
    }
  }
}