class claymore::service inherits claymore {

  file { '/lib/systemd/system/claymore.service':
    ensure  => file,
    content => template('claymore/claymore.service.erb'),
    notify  => Exec['claymore:daemon-reload']
  }

  exec {'claymore:daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    notify      => Service['claymore']
  }

  service { 'claymore':
    ensure => $claymore::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $claymore::enabled
  }
}